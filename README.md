# Pokedex (Pedro Henrique Queiroz Miranda - 15/0144474)

Feito na IDE Eclipse, utilizando a biblioteca java swing.

## Login/Cadastro

Ao iniciar o programa uma interface de Login aparecerá:

![N|Solid](https://gitlab.com/pmiranda7410/ep2/raw/master/fotosREADME/login.png)

no qual, caso você digite algo nos campos de "nome de usuário" e "senha" que não exista no banco de dados, lhe mostrará uma tela de erro com nome ou senha inválidos:

![N|Solid](https://gitlab.com/pmiranda7410/ep2/raw/master/fotosREADME/login-erro.png)

Na aba cadastro tem o espaços para colocar o nome real do usuário (no qual, pode ser colcoado o nome completo) e o nome de acesso e senha:

![N|Solid](https://gitlab.com/pmiranda7410/ep2/raw/master/fotosREADME/cadastro.png)

Caso você aperte o botão sem ter preenchido todos os espaços, ele dará uma mensagem de erro:

![N|Solid](https://gitlab.com/pmiranda7410/ep2/raw/master/fotosREADME/erro-cadastrar.png)

Caso faça o cadastro corretamente, você receberá uma mensagem de sucesso, um pokemon aleatório para seu inventário e poderá efetuar o login, como no exemplo abaixo:

![N|Solid](https://gitlab.com/pmiranda7410/ep2/raw/master/fotosREADME/login-efetuado.png)


## POKEDEX

Depois de efetuar o login, a primeira aba que aparecer será a do seu perfil com seu nome e o seu pokemon, que foi sorteado aleatoriamente:

![N|Solid](https://gitlab.com/pmiranda7410/ep2/raw/master/fotosREADME/menu-inicial-pokedex.png)

Caso queira selecionar seu pokemon e ver seus dados, selecione na lista e clique no botão "ver pokemon", se você não selecionar nenhum pokemon ele dará o seguinte erro:

![N|Solid](https://gitlab.com/pmiranda7410/ep2/raw/master/fotosREADME/apertar-botao-erro.png)

Se você selecionou seu pokemon e clicou no botão ele deve gerar uma janela com os dados do seu pokemon:

![N|Solid](https://gitlab.com/pmiranda7410/ep2/raw/master/fotosREADME/pagina-meus-pokemons.png)

Na aba lista pokemon, você encontra a lista com todos os pokemons com 2 botões (botao para se pesquisar pelo nome e o botão para ver os dados de um pokemon selecionado na lista):

![N|Solid](https://gitlab.com/pmiranda7410/ep2/raw/master/fotosREADME/aba-lista-todosPokemons.png)

Caso você aperte os botões sem escrever nada ou sem selecionar nenhum pokemon ele dará uma janela de erro:

![N|Solid](https://gitlab.com/pmiranda7410/ep2/raw/master/fotosREADME/erro-nenhum-pokemons-selecionado.png)

![N|Solid](https://gitlab.com/pmiranda7410/ep2/raw/master/fotosREADME/botao-por-nome-erro.png)

Caso você selecione um pokemon da lista e clique no botão ele te dará os dados de tal pokemon:

![N|Solid](https://gitlab.com/pmiranda7410/ep2/raw/master/fotosREADME/botao-depois-de-selecionado.png)

Na última aba, "outros treinadores" você consegue ver uma lista com todos os usuários cadastrados e pode efetuar uma busca colocando seu nome de usuário no espaço de texto e clicando no botão:

![N|Solid](https://gitlab.com/pmiranda7410/ep2/raw/master/fotosREADME/aba-outros-treinadores.png)

caso você nao digite nenhum treinador e clique no botão ele vai abrir a seguinte mensagem:

![N|Solid](https://gitlab.com/pmiranda7410/ep2/raw/master/fotosREADME/botao-buscar-usuario-erro.png)

Caso você escreva um nome e aperte o botão, ele reabrirá a janela pokedex e se você for novamente para a aba "outros treinadores" você verá os dados do treinador selecionado(No caso, selecionei o treinador pedrojams):

![N|Solid](https://gitlab.com/pmiranda7410/ep2/raw/master/fotosREADME/botao-outros-treinadores-selecionado.png)





