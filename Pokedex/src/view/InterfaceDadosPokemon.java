package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import model.Pokemon;




public class InterfaceDadosPokemon extends javax.swing.JFrame {

	
	
	/*InterfaceDadosPokemon(String nomePagina, int posicaoPokemon, String nomeUsuario, String nome, String[] types){
		super(nomePagina);
		
		criarMenu( nomeUsuario, nome);
		initComponents(types);
	}*/
	
	
	    public InterfaceDadosPokemon(String nomePagina, int indice, String nomeUsuario, String nome, String acessPokemonType, String[] listaMoves, String urlImage, String senha) {
	    	super(nomePagina);
	    	this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

			criarMenu( nomeUsuario, nome, senha);
			initComponents(acessPokemonType, nomePagina, listaMoves, urlImage);
	}


		@SuppressWarnings("unchecked")
                      
	    private void initComponents(String types, String nomePokemon, String[] listaMoves, String urlImage) {
			
			if(urlImage != "") {
			    String path = urlImage;
			    URL url = null;
				try {
					url = new URL(path);
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    BufferedImage image = null;
				try {
					image = ImageIO.read(url);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				labelImagemINUTIL = new javax.swing.JLabel(new ImageIcon(image));
			}
			else {
				labelImagemINUTIL = new javax.swing.JLabel("NAO EXISTE");
			}
			    //JLabel label = new JLabel(new ImageIcon(image));
	        panelIMAGEM = new javax.swing.JPanel(new BorderLayout(0, 0));
	        panelIMAGEM.setBorder(BorderFactory.createLineBorder(Color.BLACK));
	        panelIMAGEM.setPreferredSize(new Dimension(180,180));
	        
	        /*labelImagemINUTIL.setBorder(BorderFactory.createLineBorder(Color.BLACK));
	        panelIMAGEM.add(labelImagemINUTIL, BorderLayout.CENTER);
	        labelImagemINUTIL.setHorizontalAlignment(SwingConstants.CENTER);
	        labelImagemINUTIL.setVerticalAlignment(SwingConstants.CENTER);
	        labelImagemINUTIL.setLocation( (panelIMAGEM.getSize().width - labelImagemINUTIL.getSize().width), (0));
	        */
	        panelIMAGEM.add(BorderLayout.CENTER, labelImagemINUTIL);
	        labelNome = new javax.swing.JLabel();
	        labelType = new javax.swing.JLabel();
	        jLabel3 = new javax.swing.JLabel();
	        labelAbilidades = new javax.swing.JLabel();
	        botaoTentarCapturar = new javax.swing.JButton();
	        jScrollPane1 = new javax.swing.JScrollPane();
	        listaAbilidades = new javax.swing.JList<>();
	        nomePokemonMUDAR = new javax.swing.JLabel();
	        nomeTipoPokemonMUDAR = new javax.swing.JLabel();

	        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
	        if(urlImage == "") {
	        labelImagemINUTIL.setText("NAO EXISTE FOTO!");
	        }
	        else {
	        labelImagemINUTIL.setText("");
	        }
	        javax.swing.GroupLayout panelIMAGEMLayout = new javax.swing.GroupLayout(panelIMAGEM);
	        panelIMAGEM.setLayout(panelIMAGEMLayout);
	        panelIMAGEMLayout.setHorizontalGroup(
	            panelIMAGEMLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(panelIMAGEMLayout.createSequentialGroup()
	                .addGap(0, 90, 180)
	                .addComponent(labelImagemINUTIL)
	                .addContainerGap(90, Short.MAX_VALUE))
	        );
	        panelIMAGEMLayout.setVerticalGroup(
	            panelIMAGEMLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(panelIMAGEMLayout.createSequentialGroup()
	                .addGap(0, 90, 180)
	                .addComponent(labelImagemINUTIL)
	                .addContainerGap(90, Short.MAX_VALUE))
	        );

	        labelNome.setText("Nome:");
	        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	        labelType.setText("Type:");

	        labelAbilidades.setText("Possiveis Habilidades");

	        botaoTentarCapturar.setText("Tentar Capturar!");
	        botaoTentarCapturar.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {

					botaoTentarCapturar(evt);

				}

				private void botaoTentarCapturar(ActionEvent evt) {
					
					JOptionPane.showMessageDialog(null, "Funcionalidade sendo implementada para atualizações futuras!", "Desculpe", JOptionPane.PLAIN_MESSAGE);
					

				}
			});
	        
	        
	        listaAbilidades.setModel(new javax.swing.AbstractListModel<String>() {
	            String[] strings = listaMoves;
	            public int getSize() { return strings.length; }
	            public String getElementAt(int i) { return strings[i]; }
	        });
	        jScrollPane1.setViewportView(listaAbilidades);

	        nomePokemonMUDAR.setText(nomePokemon.toUpperCase());

	        nomeTipoPokemonMUDAR.setText(types);

	        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
	        getContentPane().setLayout(layout);
	        layout.setHorizontalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(panelIMAGEM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addGap(18, 18, 18)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addGroup(layout.createSequentialGroup()
	                        .addComponent(botaoTentarCapturar, javax.swing.GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE)
	                        .addContainerGap())
	                    .addGroup(layout.createSequentialGroup()
	                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                            .addGroup(layout.createSequentialGroup()
	                                .addComponent(labelNome)
	                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                                .addComponent(nomePokemonMUDAR))
	                            .addGroup(layout.createSequentialGroup()
	                                .addComponent(labelType)
	                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                                .addComponent(nomeTipoPokemonMUDAR)
	                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                                .addComponent(jLabel3)))
	                        .addGap(0, 0, Short.MAX_VALUE))
	                    .addGroup(layout.createSequentialGroup()
	                        .addComponent(labelAbilidades)
	                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                        .addComponent(jScrollPane1))))
	        );
	        layout.setVerticalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
	                .addContainerGap()
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(panelIMAGEM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                    .addGroup(layout.createSequentialGroup()
	                        .addGap(12, 12, 12)
	                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                            .addComponent(labelNome)
	                            .addComponent(nomePokemonMUDAR))
	                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                            .addGroup(layout.createSequentialGroup()
	                                .addGap(23, 23, 23)
	                                .addComponent(jLabel3)
	                                .addGap(30, 30, 30))
	                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
	                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                                    .addComponent(labelType)
	                                    .addComponent(nomeTipoPokemonMUDAR))
	                                .addGap(18, 18, 18)))
	                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                            .addComponent(labelAbilidades)
	                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
	                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 73, Short.MAX_VALUE)
	                        .addComponent(botaoTentarCapturar, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)))
	                .addGap(62, 62, 62))
	        );
	      
	        
	        pack();
	    }// </editor-fold> 
	    
	    private javax.swing.JButton botaoTentarCapturar;
	    private javax.swing.JLabel jLabel3;
	    private javax.swing.JScrollPane jScrollPane1;
	    private javax.swing.JLabel labelAbilidades;
	    private javax.swing.JLabel labelImagemINUTIL;
	    private javax.swing.JLabel labelNome;
	    private javax.swing.JLabel labelType;
	    private javax.swing.JList<String> listaAbilidades;
	    private javax.swing.JLabel nomePokemonMUDAR;
	    private javax.swing.JLabel nomeTipoPokemonMUDAR;
	    private javax.swing.JPanel panelIMAGEM;
	    private javax.swing.ImageIcon icon;
	    

	public void criarMenu(String nomeUsuario, String nome, String senha) {
		
		JMenu menuVoltar = new JMenu("Voltar");
		
		VoltarAction voltarAction = new VoltarAction(nomeUsuario, nome, senha);
		
		JMenuItem menuItemVoltarLista = new JMenuItem("Voltar para Lista de Pokemons");
		menuVoltar.add(menuItemVoltarLista);
		menuItemVoltarLista.addActionListener(voltarAction);
		
		
		
		JMenuBar barraMenu = new JMenuBar();
		setJMenuBar(barraMenu);
		barraMenu.add(menuVoltar);
		
		
	}
	
		
	
	private class VoltarAction implements ActionListener{
		
		String x;
		String y;
		String z;
		VoltarAction(String nomeUsuario,String nome, String senha){
			x = nomeUsuario;
			y = nome;
			z = senha;
		}
		
		public void actionPerformed(ActionEvent event) {
		
			setVisible(false);
			InterfacePokedex pokedex = new InterfacePokedex(x, y, z);
			pokedex.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			pokedex.setSize(530,260);
			//pokedex.setVisible(true);
			
		}
		
	}
	
	
	
	
}
