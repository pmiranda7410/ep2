package view;

import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import model.ListaPokemons;
import model.ListaTreinadores;
import model.Pokemon;

public class InterfacePokedex extends javax.swing.JFrame {

	/**
	 * Creates new form InterfacePokedex
	 */

	ListaPokemons listaPokemons = new ListaPokemons();
	ListaTreinadores listaTreinadores = new ListaTreinadores();
	String[] nomeCadaPokemon = listaPokemons.nomeCadaPokemon();
	String nomeUsuarioSelecionado = " ";
	Pokemon pokemon = new Pokemon();
	boolean tipo = false;

	// PRIMEIRO CONSTUTOR (1)
	public InterfacePokedex(String Usuario, String nome, String senha) {
		super("Pokedex");

		pokedexInicial(Usuario, nome, senha);
	}

	@SuppressWarnings({ "unchecked", "null" })
                        
    private void pokedexInicial(String nomeUsuario, String nome, String senha) {

        opcoesPerfilEListaPokemons = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jPanelOutrosTreinadores = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        listaPokemonsPerfil = new javax.swing.JList<>();
        caixaTextoTipo= new javax.swing.JTextField();
        botaoPerfil = new javax.swing.JButton();
        botaoTipo = new javax.swing.JButton();
        labelParaNomeUsuario = new javax.swing.JLabel();
        mudarNomeUsuario = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listaPokemonsAPI = new javax.swing.JList<>(nomeCadaPokemon);
        botaoListaAPI = new javax.swing.JButton();
        listaUsuariosOutrosTreinadores = new javax.swing.JScrollPane();
        listaUsuarios = new javax.swing.JList<>();
        textFieldBuscarUsuario = new javax.swing.JTextField();
        botaoBuscarUsuario = new javax.swing.JButton();
        labelBuscarUsuario = new javax.swing.JLabel();
        listaPokemonsOutrosTreinadores = new javax.swing.JScrollPane();
        listaPokemonsOutroUsuario = new javax.swing.JList<>();
        labelPokemonsDo_NOMEUSUARIO = new javax.swing.JLabel();

        
        
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        
        listaPokemonsPerfil.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = listaTreinadores.listaMeusPokemons(listaTreinadores.numeroIntegranteAcessou(nomeUsuario, senha));
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(listaPokemonsPerfil);

        
        
        botaoPerfil.setText("Ver Pokemon");
        botaoPerfil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
               
					botaoPerfil(evt);
				
            }

			private void botaoPerfil(ActionEvent evt) {
				
				/*String nomePokemon= listaPokemonsPerfil.getSelectedValue();
				System.out.println("Nome POKEMON: " + nomePokemon);
				int numeroPokemon = pokemon.acessPokemonNumber(nomePokemon);
				System.out.println("Você selecionou o pokemon: " + numeroPokemon);*/
				String nomePokemon=  "";
				int indice = 0;
				
				boolean selecionou = false;
				if(listaPokemonsPerfil.getSelectedValue() != null ) {
					selecionou = true;
					nomePokemon= listaPokemonsPerfil.getSelectedValue();
					indice = listaPokemons.indicePokemonPorNome(nomePokemon);
					
				}
				if(selecionou == false) {
					JOptionPane.showMessageDialog(null, "Selecione o pokemon que quer ver na lista, clicando em cima do nome!", "ERRO", JOptionPane.PLAIN_MESSAGE);
					
				}
				else if(selecionou == true) {
					InterfaceDadosPokemon pokemon = new InterfaceDadosPokemon( listaPokemons.listaInteira().get(indice).getNome(),indice, nomeUsuario,nome, listaPokemons.listaInteira().get(indice).acessPokemonType(), listaPokemons.listaInteira().get(indice).acessPokemonMoves(),listaPokemons.listaInteira().get(indice).acessUrlImage(), senha);
					caixaTextoTipo.setText("");
					pokemon.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
					pokemon.setSize(530,260);
					pokemon.setVisible(true);
					
				}
				/*
				//setVisible(false);                                    String nomePagina, int indice, String nomeUsuario, String nome, String acessPokemonType, String[] listaMoves, String urlImage, String senha)
				InterfaceDadosMeuPokemon pokemon = new InterfaceDadosMeuPokemon(nomePokemon,numeroPokemon, nomeUsuario, nome, listaPokemons.listaInteira().get(numeroPokemon).acessPokemonType(), listaPokemons.listaInteira().get(numeroPokemon).acessPokemonMoves(),listaPokemons.listaInteira().get(numeroPokemon).acessUrlImage(), senha);
		
				pokemon.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
				pokemon.setSize(530,260);
				pokemon.setVisible(true);
				*/
			}
        });
        
        
        labelParaNomeUsuario.setText("Nome Treinador:");
        botaoTipo.setText("Ver por nome");
        botaoTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
               
					botaoTipo(evt);
				
            }

			private void botaoTipo(ActionEvent evt) {
				
			String escolhaUsuario = caixaTextoTipo.getText().trim();
			
			
			int indice = listaPokemons.indicePokemonPorNome(escolhaUsuario);
			
			
			if( indice >= 0) {
				
				
				InterfaceDadosPokemon pokemon = new InterfaceDadosPokemon( listaPokemons.listaInteira().get(indice).getNome(),indice, nomeUsuario,nome, listaPokemons.listaInteira().get(indice).acessPokemonType(), listaPokemons.listaInteira().get(indice).acessPokemonMoves(),listaPokemons.listaInteira().get(indice).acessUrlImage(), senha);
				caixaTextoTipo.setText("");
				pokemon.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
				pokemon.setSize(530,260);
				pokemon.setVisible(true);
			}
			
			else {
					
			JOptionPane.showMessageDialog(null, "Digite um nome válido (tudo minúsculo)", "ERRO", JOptionPane.PLAIN_MESSAGE);
			caixaTextoTipo.setText("");
			}
				
				
			}
        });
        
        
        mudarNomeUsuario.setText(nome);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botaoPerfil, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(labelParaNomeUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mudarNomeUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 21, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(labelParaNomeUsuario)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mudarNomeUsuario)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(botaoPerfil, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        opcoesPerfilEListaPokemons.addTab("Perfil Treinador", jPanel2);
       
       
        listaPokemonsAPI.setModel(new javax.swing.AbstractListModel<String>() {
        	
            String[] strings = nomeCadaPokemon;
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(listaPokemonsAPI);

        jScrollPane1.add(caixaTextoTipo);
        //BOTAO DA LISTA DE POKEMONS
        botaoListaAPI.setText("Ver Pokemon");
        botaoListaAPI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
               
					botaoListaAPI(evt);
				
            }

			private void botaoListaAPI(ActionEvent evt) {
				
				String nomePokemon=  "";
				int indice = 0;
				
				boolean selecionou = false;
				if(listaPokemonsAPI.getSelectedValue() != null ) {
					selecionou = true;
					
					nomePokemon= listaPokemonsPerfil.getSelectedValue();
					indice = listaPokemonsAPI.getSelectedIndex();
					
				}
				if(selecionou == false) {
					JOptionPane.showMessageDialog(null, "Selecione o pokemon que quer ver na lista, clicando em cima do nome!", "ERRO", JOptionPane.PLAIN_MESSAGE);
					
				}
				else if(selecionou == true) {
					InterfaceDadosPokemon pokemon = new InterfaceDadosPokemon( listaPokemons.listaInteira().get(indice).getNome(),indice, nomeUsuario,nome, listaPokemons.listaInteira().get(indice).acessPokemonType(), listaPokemons.listaInteira().get(indice).acessPokemonMoves(),listaPokemons.listaInteira().get(indice).acessUrlImage(), senha);
					caixaTextoTipo.setText("");
					pokemon.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
					pokemon.setSize(530,260);
					pokemon.setVisible(true);
					
				}
				
			}
        });
        
        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(botaoListaAPI, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(caixaTextoTipo, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)
                        .addGap(4, 4, 4)
                        .addComponent(botaoTipo)
                        .addGap(94, 94, 94))))
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(0, 0, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(caixaTextoTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(botaoTipo))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(botaoListaAPI, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap())
            );
        
        listaUsuarios.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = listaTreinadores.getListaOutrosTreinadores();
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        listaUsuariosOutrosTreinadores.setViewportView(listaUsuarios);
        
        listaPokemonsOutroUsuario.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "------" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        listaPokemonsOutrosTreinadores.setViewportView(listaPokemonsOutroUsuario);

        opcoesPerfilEListaPokemons.addTab("Lista Pokemons", jPanel1);
        opcoesPerfilEListaPokemons.addTab("Outros Treinadores", jPanelOutrosTreinadores );
        
        botaoBuscarUsuario.setText("Buscar Usuário");
        botaoBuscarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
               
					botaoBuscarUsuario(evt);
				
            }

			private void botaoBuscarUsuario(ActionEvent evt) {
				
				System.out.println("APERTOU O BOTAO BUSCAR USUARIO" );
				String treinador = textFieldBuscarUsuario.getText().trim();
				System.out.println("%%%%%%%%%% " + treinador + "%%%%%%%%" );
				
				if(textFieldBuscarUsuario.getText().length() < 1) {
					JOptionPane.showMessageDialog(null, "Digite um dos nomes de usuário da lista!", "ERRO", JOptionPane.PLAIN_MESSAGE);
					
				}
				
				else {
				int posicaoTreinadorNaLista = listaTreinadores.numeroTreinadorEscolhidoNaLista(treinador);
				System.out.println("********** " + posicaoTreinadorNaLista + " ***************" );
				InterfacePokedex pokedex = new InterfacePokedex(nomeUsuario, nome, senha, posicaoTreinadorNaLista,"");
				
				caixaTextoTipo.setText("");
				pokedex.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				pokedex.setSize(530,260);
				setVisible(false);
				pokedex.setVisible(true);
				}
				
			}
        });

        labelBuscarUsuario.setText("Buscar Usuario:");
        
        labelPokemonsDo_NOMEUSUARIO.setText("DIGITE O NOME DE USUARIO E APERTE O BOTAO");
        
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(opcoesPerfilEListaPokemons)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(opcoesPerfilEListaPokemons)
        );
        

        javax.swing.GroupLayout jPanelOutrosTreinadoresLayout = new javax.swing.GroupLayout(jPanelOutrosTreinadores);
        jPanelOutrosTreinadores.setLayout(jPanelOutrosTreinadoresLayout);
        jPanelOutrosTreinadoresLayout.setHorizontalGroup(
        		jPanelOutrosTreinadoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelOutrosTreinadoresLayout.createSequentialGroup()
                    .addGroup(jPanelOutrosTreinadoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelOutrosTreinadoresLayout.createSequentialGroup()
                            .addGroup(jPanelOutrosTreinadoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(listaUsuariosOutrosTreinadores)
                                .addComponent(textFieldBuscarUsuario)
                                .addComponent(botaoBuscarUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE))
                            .addGap(18, 18, 18)
                            .addComponent(labelPokemonsDo_NOMEUSUARIO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addComponent(labelBuscarUsuario))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(listaPokemonsOutrosTreinadores, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
            
        );
        jPanelOutrosTreinadoresLayout.setVerticalGroup(
        		jPanelOutrosTreinadoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelOutrosTreinadoresLayout.createSequentialGroup()
                        .addGroup(jPanelOutrosTreinadoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelOutrosTreinadoresLayout.createSequentialGroup()
                                .addComponent(listaUsuariosOutrosTreinadores, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(labelBuscarUsuario)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(textFieldBuscarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(botaoBuscarUsuario))
                            .addGroup(jPanelOutrosTreinadoresLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(labelPokemonsDo_NOMEUSUARIO)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addComponent(listaPokemonsOutrosTreinadores, javax.swing.GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE)
                
           
        );
        pack();
    }// </editor-fold>

	// COMPONENTE DO SEGUNDO CONTRUTOR
	// POLIMORFISMO DE SOBRESCRITA

	public InterfacePokedex(String Usuario, String nome, String senha, String botaoPorNome) {
		super("Pokedex");

		dadosPokemons(Usuario, nome, senha, botaoPorNome);
	}

	// SEGUNDO CONSTUTOR (2)

	private void dadosPokemons(String nomeUsuario, String nome, String senha, String pokemonsDoTipo) {

		opcoesPerfilEListaPokemons = new javax.swing.JTabbedPane();
		jPanel2 = new javax.swing.JPanel();
		jPanelOutrosTreinadores = new javax.swing.JPanel();
		jScrollPane2 = new javax.swing.JScrollPane();
		listaPokemonsPerfil = new javax.swing.JList<>();
		caixaTextoTipo = new javax.swing.JTextField();
		botaoPerfil = new javax.swing.JButton();
		botaoTipo = new javax.swing.JButton();
		labelParaNomeUsuario = new javax.swing.JLabel();
		mudarNomeUsuario = new javax.swing.JLabel();
		jPanel1 = new javax.swing.JPanel();
		jScrollPane1 = new javax.swing.JScrollPane();
		// listaPokemonsAPI = new javax.swing.JList<>(nomeCadaPokemon);
		System.out.println("GHEGOOOOU 1");
		botaoListaAPI = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		listaPokemonsPerfil.setModel(new javax.swing.AbstractListModel<String>() {
			String[] strings = listaTreinadores
					.listaMeusPokemons(listaTreinadores.numeroIntegranteAcessou(nomeUsuario, senha));

			public int getSize() {
				return strings.length;
			}

			public String getElementAt(int i) {
				return strings[i];
			}
		});
		jScrollPane2.setViewportView(listaPokemonsPerfil);

		botaoPerfil.setText("Ver Pokemon");
		botaoPerfil.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				botaoPerfil(evt);

			}

			private void botaoPerfil(ActionEvent evt) {
				String nomePokemon=  "";
				int indice = 0;
				
				boolean selecionou = false;
				if(listaPokemonsPerfil.getSelectedValue() != null ) {
					selecionou = true;
					nomePokemon= listaPokemonsPerfil.getSelectedValue();
					indice = listaPokemons.indicePokemonPorNome(nomePokemon);
					
				}
				if(selecionou == false) {
					JOptionPane.showMessageDialog(null, "Selecione o pokemon que quer ver na lista, clicando em cima do nome!", "ERRO", JOptionPane.PLAIN_MESSAGE);
					
				}
				else if(selecionou == true) {
					InterfaceDadosPokemon pokemon = new InterfaceDadosPokemon( listaPokemons.listaInteira().get(indice).getNome(),indice, nomeUsuario,nome, listaPokemons.listaInteira().get(indice).acessPokemonType(), listaPokemons.listaInteira().get(indice).acessPokemonMoves(),listaPokemons.listaInteira().get(indice).acessUrlImage(), senha);
					caixaTextoTipo.setText("");
					pokemon.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
					pokemon.setSize(530,260);
					pokemon.setVisible(true);
					
				}
			}
		});
		System.out.println("GHEGOOOOU 2");
		labelParaNomeUsuario.setText("Nome Treinador:");
		 botaoTipo.setText("Ver por nome");
	        botaoTipo.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	               
						botaoTipo(evt);
					
	            }

				private void botaoTipo(ActionEvent evt) {
					
				String escolhaUsuario = caixaTextoTipo.getText().toLowerCase().trim();
				
				
				int indice = listaPokemons.indicePokemonPorNome(escolhaUsuario);
				
				
				if( indice >= 0) {
					
					
					InterfaceDadosPokemon pokemon = new InterfaceDadosPokemon( listaPokemons.listaInteira().get(indice).getNome(),indice, nomeUsuario,nome, listaPokemons.listaInteira().get(indice).acessPokemonType(), listaPokemons.listaInteira().get(indice).acessPokemonMoves(),listaPokemons.listaInteira().get(indice).acessUrlImage(), senha);
					caixaTextoTipo.setText("");
					pokemon.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
					pokemon.setSize(530,260);
					pokemon.setVisible(true);
				}
				
				else {
						
				JOptionPane.showMessageDialog(null, "Digite um nome válido ou tudo minúsculo", "ERRO", JOptionPane.PLAIN_MESSAGE);
				caixaTextoTipo.setText("");
				}
					
					
				}
	        });
	        
		System.out.println("GHEGOOOOU 3");
		mudarNomeUsuario.setText(nome);

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
		jPanel2.setLayout(jPanel2Layout);
		jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(botaoPerfil, javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)

								.addGroup(jPanel2Layout.createSequentialGroup().addGroup(jPanel2Layout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
										.addComponent(labelParaNomeUsuario, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(mudarNomeUsuario, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										.addGap(0, 21, Short.MAX_VALUE)))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 199,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addContainerGap()));
		jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
								.addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING,
										javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
								.addGroup(jPanel2Layout.createSequentialGroup().addComponent(labelParaNomeUsuario)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(mudarNomeUsuario)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(botaoPerfil, javax.swing.GroupLayout.PREFERRED_SIZE, 37,
												javax.swing.GroupLayout.PREFERRED_SIZE)))
						.addContainerGap()));

		opcoesPerfilEListaPokemons.addTab("Perfil Treinador", jPanel2);

		System.out.println("GHEGOOOOU 4");
		listaPokemonsAPI.setModel(new javax.swing.AbstractListModel<String>() {

			String[] strings = listaPokemons.nomeCadaPokemonPorTipo(pokemonsDoTipo);

			public int getSize() {
				return strings.length;
			}

			public String getElementAt(int i) {
				return strings[i];
			}
		});
		jScrollPane1.setViewportView(listaPokemonsAPI);

		jScrollPane1.add(caixaTextoTipo);
		// BOTAO DA LISTA DE POKEMONS
		botaoListaAPI.setText("Ver Pokemon");
		botaoListaAPI.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				botaoListaAPI(evt);

			}

			private void botaoListaAPI(ActionEvent evt) {
				String nomePokemon=  "";
				int indice = 0;
				
				boolean selecionou = false;
				if(listaPokemonsAPI.getSelectedValue() != null ) {
					selecionou = true;
					
					nomePokemon= listaPokemonsPerfil.getSelectedValue();
					indice = listaPokemonsAPI.getSelectedIndex();
					
				}
				if(selecionou == false) {
					JOptionPane.showMessageDialog(null, "Selecione o pokemon que quer ver na lista, clicando em cima do nome!", "ERRO", JOptionPane.PLAIN_MESSAGE);
					
				}
				else if(selecionou == true) {
					InterfaceDadosPokemon pokemon = new InterfaceDadosPokemon( listaPokemons.listaInteira().get(indice).getNome(),indice, nomeUsuario,nome, listaPokemons.listaInteira().get(indice).acessPokemonType(), listaPokemons.listaInteira().get(indice).acessPokemonMoves(),listaPokemons.listaInteira().get(indice).acessUrlImage(), senha);
					caixaTextoTipo.setText("");
					pokemon.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
					pokemon.setSize(530,260);
					pokemon.setVisible(true);
					
				}
				
				
			}
		});
		System.out.println("GHEGOOOOU 5");
		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup().addContainerGap()
						.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 210,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(jPanel1Layout.createSequentialGroup()
										.addComponent(botaoListaAPI, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addContainerGap())
								.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
										jPanel1Layout.createSequentialGroup().addGap(2, 2, 2)
												.addComponent(caixaTextoTipo, javax.swing.GroupLayout.DEFAULT_SIZE, 134,
														Short.MAX_VALUE)
												.addGap(4, 4, 4).addComponent(botaoTipo).addGap(94, 94, 94)))));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
								.addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
								.addGroup(jPanel1Layout.createSequentialGroup().addGap(0, 0, Short.MAX_VALUE)
										.addGroup(jPanel1Layout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(caixaTextoTipo, javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(botaoTipo))
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(botaoListaAPI, javax.swing.GroupLayout.PREFERRED_SIZE, 41,
												javax.swing.GroupLayout.PREFERRED_SIZE)))
						.addContainerGap()));

		opcoesPerfilEListaPokemons.addTab("Lista Pokemons", jPanel1);
		opcoesPerfilEListaPokemons.addTab("Outros Treinadores", jPanelOutrosTreinadores);

		listaUsuarios.setModel(new javax.swing.AbstractListModel<String>() {
			String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };

			public int getSize() {
				return strings.length;
			}

			public String getElementAt(int i) {
				return strings[i];
			}
		});
		listaUsuariosOutrosTreinadores.setViewportView(listaUsuarios);

		botaoBuscarUsuario.setText("Buscar Usuário");
		botaoBuscarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
               
					botaoBuscarUsuario(evt);
				
            }

			private void botaoBuscarUsuario(ActionEvent evt) {
				
				System.out.println("APERTOU O BOTAO BUSCAR USUARIO" );
				String treinador = textFieldBuscarUsuario.getText().trim();
				System.out.println("%%%%%%%%%% " + treinador + "%%%%%%%%" );
				
				if(textFieldBuscarUsuario.getText().length() < 1) {
					JOptionPane.showMessageDialog(null, "Digite um dos nomes de usuário da lista!", "ERRO", JOptionPane.PLAIN_MESSAGE);
					
				}
				
				else {
				int posicaoTreinadorNaLista = listaTreinadores.numeroTreinadorEscolhidoNaLista(treinador);
				System.out.println("********** " + posicaoTreinadorNaLista + " ***************" );
				InterfacePokedex pokedex = new InterfacePokedex(nomeUsuario, nome, senha, posicaoTreinadorNaLista,"");
				
				caixaTextoTipo.setText("");
				pokedex.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				pokedex.setSize(530,260);
				setVisible(false);
				pokedex.setVisible(true);
				}
				
			}
        });
		
		labelBuscarUsuario.setText("Buscar Usuario:");

		labelPokemonsDo_NOMEUSUARIO.setText("Pokemons do -NAME-:");

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(opcoesPerfilEListaPokemons));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(opcoesPerfilEListaPokemons));

		javax.swing.GroupLayout jPanelOutrosTreinadoresLayout = new javax.swing.GroupLayout(jPanelOutrosTreinadores);
		jPanelOutrosTreinadores.setLayout(jPanelOutrosTreinadoresLayout);
		jPanelOutrosTreinadoresLayout.setHorizontalGroup(jPanelOutrosTreinadoresLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanelOutrosTreinadoresLayout.createSequentialGroup().addGroup(jPanelOutrosTreinadoresLayout
						.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(jPanelOutrosTreinadoresLayout.createSequentialGroup()
								.addGroup(jPanelOutrosTreinadoresLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
										.addComponent(listaUsuariosOutrosTreinadores)
										.addComponent(textFieldBuscarUsuario).addComponent(botaoBuscarUsuario,
												javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE))
								.addGap(18, 18, 18).addComponent(labelPokemonsDo_NOMEUSUARIO,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE))
						.addComponent(labelBuscarUsuario))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(listaPokemonsOutrosTreinadores, javax.swing.GroupLayout.PREFERRED_SIZE, 130,
								javax.swing.GroupLayout.PREFERRED_SIZE))

		);
		jPanelOutrosTreinadoresLayout.setVerticalGroup(jPanelOutrosTreinadoresLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanelOutrosTreinadoresLayout.createSequentialGroup().addGroup(jPanelOutrosTreinadoresLayout
						.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(jPanelOutrosTreinadoresLayout.createSequentialGroup()
								.addComponent(listaUsuariosOutrosTreinadores, javax.swing.GroupLayout.PREFERRED_SIZE, 0,
										Short.MAX_VALUE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(labelBuscarUsuario)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(textFieldBuscarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(botaoBuscarUsuario))
						.addGroup(jPanelOutrosTreinadoresLayout.createSequentialGroup().addContainerGap()
								.addComponent(labelPokemonsDo_NOMEUSUARIO).addGap(0, 0, Short.MAX_VALUE)))
						.addContainerGap())
				.addComponent(listaPokemonsOutrosTreinadores, javax.swing.GroupLayout.DEFAULT_SIZE, 304,
						Short.MAX_VALUE)

		);

		pack();
	}// </editor-fold>

	// TERCEIRO CONSTRUTOR (3)

	public InterfacePokedex(String nomeUsuario, String nome, String senha, int numOutroTreinador, String diferencial) {
		super("Pokedex");
		System.out.println("GHEGOOOOU 0");
		abaOutrosTreinadores(nomeUsuario, nome, senha, numOutroTreinador);
	}

	@SuppressWarnings({ "unchecked", "null" })

	private void abaOutrosTreinadores(String nomeUsuario, String nome, String senha, int numOutroTreinador) {

		opcoesPerfilEListaPokemons = new javax.swing.JTabbedPane();
		jPanel2 = new javax.swing.JPanel();
		jPanelOutrosTreinadores = new javax.swing.JPanel();
		jScrollPane2 = new javax.swing.JScrollPane();
		listaPokemonsPerfil = new javax.swing.JList<>();
		caixaTextoTipo = new javax.swing.JTextField();
		botaoPerfil = new javax.swing.JButton();
		botaoTipo = new javax.swing.JButton();
		labelParaNomeUsuario = new javax.swing.JLabel();
		mudarNomeUsuario = new javax.swing.JLabel();
		jPanel1 = new javax.swing.JPanel();
		jScrollPane1 = new javax.swing.JScrollPane();
		listaPokemonsAPI = new javax.swing.JList<>(nomeCadaPokemon);
		botaoListaAPI = new javax.swing.JButton();
		listaUsuariosOutrosTreinadores = new javax.swing.JScrollPane();
		listaUsuarios = new javax.swing.JList<>();
		textFieldBuscarUsuario = new javax.swing.JTextField();
		botaoBuscarUsuario = new javax.swing.JButton();
		labelBuscarUsuario = new javax.swing.JLabel();
		listaPokemonsOutrosTreinadores = new javax.swing.JScrollPane();
		listaPokemonsOutroUsuario = new javax.swing.JList<>();
		labelPokemonsDo_NOMEUSUARIO = new javax.swing.JLabel();

		System.out.println("CHEGOOOU *************** 1");
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		listaPokemonsPerfil.setModel(new javax.swing.AbstractListModel<String>() {
			String[] strings = listaTreinadores
					.listaMeusPokemons(listaTreinadores.numeroIntegranteAcessou(nomeUsuario, senha));

			public int getSize() {
				return strings.length;
			}

			public String getElementAt(int i) {
				return strings[i];
			}
		});
		jScrollPane2.setViewportView(listaPokemonsPerfil);

		System.out.println("CHEGOOOU *************** 2");
		botaoPerfil.setText("Ver Pokemon");
		botaoPerfil.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				botaoPerfil(evt);

			}

			private void botaoPerfil(ActionEvent evt) {

				String nomePokemon=  "";
				int indice = 0;
				
				boolean selecionou = false;
				if(listaPokemonsPerfil.getSelectedValue() != null ) {
					selecionou = true;
					nomePokemon= listaPokemonsPerfil.getSelectedValue();
					indice = listaPokemons.indicePokemonPorNome(nomePokemon);
					
				}
				if(selecionou == false) {
					JOptionPane.showMessageDialog(null, "Selecione o pokemon que quer ver na lista, clicando em cima do nome!", "ERRO", JOptionPane.PLAIN_MESSAGE);
					
				}
				else if(selecionou == true) {
					InterfaceDadosPokemon pokemon = new InterfaceDadosPokemon( listaPokemons.listaInteira().get(indice).getNome(),indice, nomeUsuario,nome, listaPokemons.listaInteira().get(indice).acessPokemonType(), listaPokemons.listaInteira().get(indice).acessPokemonMoves(),listaPokemons.listaInteira().get(indice).acessUrlImage(), senha);
					caixaTextoTipo.setText("");
					pokemon.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
					pokemon.setSize(530,260);
					pokemon.setVisible(true);
					
				}
			}
		});

		labelParaNomeUsuario.setText("Nome Treinador:");
		botaoTipo.setText("Ver por nome");
		botaoTipo.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				botaoTipo(evt);

			}

			private void botaoTipo(ActionEvent evt) {
				
				String escolhaUsuario = caixaTextoTipo.getText().toLowerCase().trim();
				
				
				int indice = listaPokemons.indicePokemonPorNome(escolhaUsuario);
				
				
				if( indice >= 0) {
					
					
					InterfaceDadosPokemon pokemon = new InterfaceDadosPokemon( listaPokemons.listaInteira().get(indice).getNome(),indice, nomeUsuario,nome, listaPokemons.listaInteira().get(indice).acessPokemonType(), listaPokemons.listaInteira().get(indice).acessPokemonMoves(),listaPokemons.listaInteira().get(indice).acessUrlImage(), senha);
					caixaTextoTipo.setText("");
					pokemon.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
					pokemon.setSize(530,260);
					pokemon.setVisible(true);
				}
				
				else {
						
				JOptionPane.showMessageDialog(null, "Digite um nome válido ou tudo minúsculo", "ERRO", JOptionPane.PLAIN_MESSAGE);
				caixaTextoTipo.setText("");
				}

			}
		});

		mudarNomeUsuario.setText(nome);

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
		jPanel2.setLayout(jPanel2Layout);
		jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(botaoPerfil, javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)

								.addGroup(jPanel2Layout.createSequentialGroup().addGroup(jPanel2Layout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
										.addComponent(labelParaNomeUsuario, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(mudarNomeUsuario, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										.addGap(0, 21, Short.MAX_VALUE)))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 199,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addContainerGap()));
		jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
								.addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING,
										javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
								.addGroup(jPanel2Layout.createSequentialGroup().addComponent(labelParaNomeUsuario)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(mudarNomeUsuario)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(botaoPerfil, javax.swing.GroupLayout.PREFERRED_SIZE, 37,
												javax.swing.GroupLayout.PREFERRED_SIZE)))
						.addContainerGap()));

		opcoesPerfilEListaPokemons.addTab("Perfil Treinador", jPanel2);
		System.out.println("CHEGOOOU *************** 3");

		listaPokemonsAPI.setModel(new javax.swing.AbstractListModel<String>() {

			String[] strings = nomeCadaPokemon;

			public int getSize() {
				return strings.length;
			}

			public String getElementAt(int i) {
				return strings[i];
			}
		});
		jScrollPane1.setViewportView(listaPokemonsAPI);

		jScrollPane1.add(caixaTextoTipo);
		// BOTAO DA LISTA DE POKEMONS
		botaoListaAPI.setText("Ver Pokemon");
		botaoListaAPI.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				botaoListaAPI(evt);

			}

			private void botaoListaAPI(ActionEvent evt) {

				String nomePokemon=  "";
				int indice = 0;
				
				boolean selecionou = false;
				if(listaPokemonsAPI.getSelectedValue() != null ) {
					selecionou = true;
					
					nomePokemon= listaPokemonsPerfil.getSelectedValue();
					indice = listaPokemonsAPI.getSelectedIndex();
					
				}
				if(selecionou == false) {
					JOptionPane.showMessageDialog(null, "Selecione o pokemon que quer ver na lista, clicando em cima do nome!", "ERRO", JOptionPane.PLAIN_MESSAGE);
					
				}
				else if(selecionou == true) {
					InterfaceDadosPokemon pokemon = new InterfaceDadosPokemon( listaPokemons.listaInteira().get(indice).getNome(),indice, nomeUsuario,nome, listaPokemons.listaInteira().get(indice).acessPokemonType(), listaPokemons.listaInteira().get(indice).acessPokemonMoves(),listaPokemons.listaInteira().get(indice).acessUrlImage(), senha);
					caixaTextoTipo.setText("");
					pokemon.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
					pokemon.setSize(530,260);
					pokemon.setVisible(true);
					
				}
			}
		});

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup().addContainerGap()
						.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 210,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(jPanel1Layout.createSequentialGroup()
										.addComponent(botaoListaAPI, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addContainerGap())
								.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
										jPanel1Layout.createSequentialGroup().addGap(2, 2, 2)
												.addComponent(caixaTextoTipo, javax.swing.GroupLayout.DEFAULT_SIZE, 134,
														Short.MAX_VALUE)
												.addGap(4, 4, 4).addComponent(botaoTipo).addGap(94, 94, 94)))));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
								.addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
								.addGroup(jPanel1Layout.createSequentialGroup().addGap(0, 0, Short.MAX_VALUE)
										.addGroup(jPanel1Layout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(caixaTextoTipo, javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(botaoTipo))
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(botaoListaAPI, javax.swing.GroupLayout.PREFERRED_SIZE, 41,
												javax.swing.GroupLayout.PREFERRED_SIZE)))
						.addContainerGap()));

		System.out.println("CHEGOOOU *************** 4");
		listaUsuarios.setModel(new javax.swing.AbstractListModel<String>() {
			String[] strings = listaTreinadores.getListaOutrosTreinadores();

			public int getSize() {
				return strings.length;
			}

			public String getElementAt(int i) {
				return strings[i];
			}
		});
		listaUsuariosOutrosTreinadores.setViewportView(listaUsuarios);

		System.out.println("CHEGOOOU *************** 5");
		System.out.println(
				"NUMERO DA LISTA: " + listaTreinadores.getListaTreinadores().get(numOutroTreinador).getNomeUsuario()
						+ " numero: " + numOutroTreinador);
		listaPokemonsOutroUsuario.setModel(new javax.swing.AbstractListModel<String>() {
			String[] strings = listaTreinadores.getListaTreinadores().get(numOutroTreinador).getPokemonsList();

			public int getSize() {
				return strings.length;
			}

			public String getElementAt(int i) {
				return strings[i];
			}
		});
		listaPokemonsOutrosTreinadores.setViewportView(listaPokemonsOutroUsuario);

		opcoesPerfilEListaPokemons.addTab("Lista Pokemons", jPanel1);
		opcoesPerfilEListaPokemons.addTab("Outros Treinadores", jPanelOutrosTreinadores);
		System.out.println("CHEGOOOU *************** 6");
		botaoBuscarUsuario.setText("Buscar Usuário");
		botaoBuscarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
               
					botaoBuscarUsuario(evt);
				
            }

			private void botaoBuscarUsuario(ActionEvent evt) {
				
				System.out.println("APERTOU O BOTAO BUSCAR USUARIO" );
				String treinador = textFieldBuscarUsuario.getText().trim();
				System.out.println("%%%%%%%%%% " + treinador + "%%%%%%%%" );
				
				if(textFieldBuscarUsuario.getText().length() < 1) {
					JOptionPane.showMessageDialog(null, "Digite um dos nomes de usuário da lista!", "ERRO", JOptionPane.PLAIN_MESSAGE);
					
				}
				
				else {
				int posicaoTreinadorNaLista = listaTreinadores.numeroTreinadorEscolhidoNaLista(treinador);
				System.out.println("********** " + posicaoTreinadorNaLista + " ***************" );
				InterfacePokedex pokedex = new InterfacePokedex(nomeUsuario, nome, senha, posicaoTreinadorNaLista,"");
				
				caixaTextoTipo.setText("");
				pokedex.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				pokedex.setSize(530,260);
				setVisible(false);
				pokedex.setVisible(true);
				}
				
			}
        });
		System.out.println("CHEGOOOU *************** 7");
		labelBuscarUsuario.setText("Buscar Usuario:");

		labelPokemonsDo_NOMEUSUARIO.setText("Lista de Pokemons do "
				+ listaTreinadores.getListaTreinadores().get(numOutroTreinador).getNome() + ": ");

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(opcoesPerfilEListaPokemons));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(opcoesPerfilEListaPokemons));

		javax.swing.GroupLayout jPanelOutrosTreinadoresLayout = new javax.swing.GroupLayout(jPanelOutrosTreinadores);
		jPanelOutrosTreinadores.setLayout(jPanelOutrosTreinadoresLayout);
		jPanelOutrosTreinadoresLayout.setHorizontalGroup(jPanelOutrosTreinadoresLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanelOutrosTreinadoresLayout.createSequentialGroup().addGroup(jPanelOutrosTreinadoresLayout
						.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(jPanelOutrosTreinadoresLayout.createSequentialGroup()
								.addGroup(jPanelOutrosTreinadoresLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
										.addComponent(listaUsuariosOutrosTreinadores)
										.addComponent(textFieldBuscarUsuario).addComponent(botaoBuscarUsuario,
												javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE))
								.addGap(18, 18, 18).addComponent(labelPokemonsDo_NOMEUSUARIO,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE))
						.addComponent(labelBuscarUsuario))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(listaPokemonsOutrosTreinadores, javax.swing.GroupLayout.PREFERRED_SIZE, 130,
								javax.swing.GroupLayout.PREFERRED_SIZE))

		);
		jPanelOutrosTreinadoresLayout.setVerticalGroup(jPanelOutrosTreinadoresLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanelOutrosTreinadoresLayout.createSequentialGroup().addGroup(jPanelOutrosTreinadoresLayout
						.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(jPanelOutrosTreinadoresLayout.createSequentialGroup()
								.addComponent(listaUsuariosOutrosTreinadores, javax.swing.GroupLayout.PREFERRED_SIZE, 0,
										Short.MAX_VALUE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(labelBuscarUsuario)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(textFieldBuscarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(botaoBuscarUsuario))
						.addGroup(jPanelOutrosTreinadoresLayout.createSequentialGroup().addContainerGap()
								.addComponent(labelPokemonsDo_NOMEUSUARIO).addGap(0, 0, Short.MAX_VALUE)))
						.addContainerGap())
				.addComponent(listaPokemonsOutrosTreinadores, javax.swing.GroupLayout.DEFAULT_SIZE, 304,
						Short.MAX_VALUE)

		);
		pack();
	}

	// Variables declaration - do not modify
	private javax.swing.JButton botaoListaAPI;
	private javax.swing.JButton botaoPerfil;
	private javax.swing.JButton botaoTipo;
	private javax.swing.JButton botaoBuscarUsuario;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JPanel jPanelOutrosTreinadores;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JLabel labelParaNomeUsuario;
	private javax.swing.JLabel labelBuscarUsuario;
	private javax.swing.JLabel labelPokemonsDo_NOMEUSUARIO;

	private javax.swing.JList<String> listaPokemonsOutroUsuario;
	private javax.swing.JScrollPane listaPokemonsOutrosTreinadores;

	private javax.swing.JList<String> listaUsuarios;
	private javax.swing.JScrollPane listaUsuariosOutrosTreinadores;
	private javax.swing.JList<String> listaPokemonsAPI;
	private javax.swing.JList<String> listaPokemonsPerfil;
	private javax.swing.JLabel mudarNomeUsuario;
	private javax.swing.JTabbedPane opcoesPerfilEListaPokemons;
	private javax.swing.JTextField caixaTextoTipo;
	private javax.swing.JTextField textFieldBuscarUsuario;

	// End of variables declaration
}
