package model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import model.ListaPokemons.Nome;


public class Pokemon {
	
	private String nome;
	private String url;
	private int posicao;
	private String type1;
	private String type2;
	
	
	public Pokemon(){}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getPosicao() {
		return posicao;
	}

	public void setPosicao(int posicao) {
		this.posicao = posicao;
	}
	
	
	public String getType1Csv() {
		
		
		String arquivoCSV = "pokemonsCSV.csv";
	    BufferedReader br = null;
	    String linha = "";
	    String csvDivisor = ",";
	    int i = 0;
	    String type_1;
	    ArrayList<Nome> pokemonsPorTipos = new ArrayList<Nome>();
	    
	    try {

	        br = new BufferedReader(new FileReader(arquivoCSV));
	        System.out.println("ABRIMOS O ARQUIVO CSV BRO");
	        while ((linha = br.readLine()) != null) {
         
	            String[] linhaCsv = linha.split(csvDivisor);
	           if( Integer.parseInt(linhaCsv[0]) == posicao ) {
	        	   this.type1 = linhaCsv[2];
	        	   this.type2 = linhaCsv[3];
	           }
	           // System.out.println("TAMO NO WHILE POSIÇAO: " + i + " * Type1: " + type[2] + " * Type2: "+type[3] + " * Nome: " + type[1]);
	        }
	        
	       

	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (br != null) {
	            try {
	                br.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	   
	    
	    
	    
	
		
		
		return type1;
	}
	
public String getType2Csv() {
		
		
		String arquivoCSV = "pokemonsCSV.csv";
	    BufferedReader br = null;
	    String linha = "";
	    String csvDivisor = ",";
	    int i = 0;
	    String type_1;
	    ArrayList<Nome> pokemonsPorTipos = new ArrayList<Nome>();
	    
	    try {

	        br = new BufferedReader(new FileReader(arquivoCSV));
	        System.out.println("ABRIMOS O ARQUIVO CSV BRO");
	        while ((linha = br.readLine()) != null) {
         
	            String[] linhaCsv = linha.split(csvDivisor);
	           if( Integer.parseInt(linhaCsv[0]) == posicao ) {
	        	   this.type1 = linhaCsv[2];
	        	   this.type2 = linhaCsv[3];
	           }
	           // System.out.println("TAMO NO WHILE POSIÇAO: " + i + " * Type1: " + type[2] + " * Type2: "+type[3] + " * Nome: " + type[1]);
	        }
	        
	       

	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (br != null) {
	            try {
	                br.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	   
	    
	    
	    
	
		
		
		return type2;
	}

	public void setType1(String type1) {
		this.type1 = type1;
	}
	
	public String getType2() {
		return type2;
	}

	public void setType2(String type2) {
		this.type2 = type2;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public int acessPokemonNumber(String nomePokemon) {
		int numeroPokemon = 0;
		
		String url = "https://pokeapi.co/api/v2/pokemon/";
	    URL obj = null;
		try {
			obj = new URL(url);
		} catch (MalformedURLException e5) {
			// TODO Auto-generated catch block
			e5.printStackTrace();
		}
	    		 
	     HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) obj.openConnection();
		} catch (IOException e4) {
			// TODO Auto-generated catch block
			e4.printStackTrace();
		}
	     // optional default is GET
	     try {
			con.setRequestMethod("GET");
		} catch (ProtocolException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
	     //add request header
	     con.setRequestProperty("User-Agent", "Mozilla/5.0");
	     int responseCode = 0;
		try {
			responseCode = con.getResponseCode();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	     System.out.println("\nSending 'GET' request to URL : " + url);
	     System.out.println("Response Code : " + responseCode);
	     BufferedReader in = null;
		try {
			in = new BufferedReader(
			         new InputStreamReader(con.getInputStream()));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		  	
		  	 
		     String inputLine;
		      
		     
		     //boolean valorNomes = false;
		    
		     StringBuffer response = new StringBuffer();
		     
		     try {
				while ((inputLine = in.readLine()) != null) {
				 	response.append(inputLine);
				 	String[] splitType = inputLine.split("\"");
				    //System.out.println(inputLine);
				 	if(splitType.length >= 4) {
				 		if(splitType[1].compareTo("name") == 0 ) {
				 			
					 		numeroPokemon++;
					 		if(splitType[3].compareTo(nomePokemon) == 0) {
					 		break;
					 		}
					 	}
				 	}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		   
		
		
		return numeroPokemon;
	}
	
	public String acessPokemonType() {
		
		String types = "";
		String type1 = "";
		String type2 = "";
		
		String url = this.url;
	    URL obj = null;
		try {
			obj = new URL(url);
		} catch (MalformedURLException e5) {
			// TODO Auto-generated catch block
			e5.printStackTrace();
		}
	    		 
	     HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) obj.openConnection();
		} catch (IOException e4) {
			// TODO Auto-generated catch block
			e4.printStackTrace();
		}
	     // optional default is GET
	     try {
			con.setRequestMethod("GET");
		} catch (ProtocolException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
	     //add request header
	     con.setRequestProperty("User-Agent", "Mozilla/5.0");
	     int responseCode = 0;
		try {
			responseCode = con.getResponseCode();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	     System.out.println("\nSending 'GET' request to URL : " + url);
	     System.out.println("Response Code : " + responseCode);
	     BufferedReader in = null;
		try {
			in = new BufferedReader(
			         new InputStreamReader(con.getInputStream()));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		  	
		  	 
		     String inputLine;
		      
		     
		     //boolean valorNomes = false;
		     int numeroLinha = 0;
		     boolean aux = false;
		     int temp = 0;
		     int temp2 = 0;
		     int typeNumber = 0;
		     StringBuffer response = new StringBuffer();
		     
		     try {
				while ((inputLine = in.readLine()) != null) {
				 	response.append(inputLine);
				 	String[] splitType = inputLine.split("\"");
				    //System.out.println(inputLine);
				 	if(splitType.length >= 2) {
				 		//System.out.println("----------");
				 		//System.out.println("Split 0: " + splitType[0]);
				 		//System.out.println("Split 1: " + splitType[1]);
				 		//System.out.println("Split 2: " + splitType[2]);
				 		//System.out.println("----------");
				 		
				 		if(splitType[1].compareTo("type") == 0 ) {
					 		aux = true;
					 		
					 		temp++;
					 	}
				 		
					 	if(temp == 2){
					 		type1 = splitType[3];
					 		setType1(splitType[3]);
					 		System.out.println(type1);
					 		temp++;
					 		}
					 	
					 	if(temp2 == 2) {
					 		type2 = splitType[3];
					 		setType2(splitType[3]);
					 		System.out.println(type2);
					 	temp2++;	
					 	}
					 	
					 	if(aux == true) {
				 			temp++;
				 			aux = false;
				 			temp2++;
				 		}
				 	}
				 	
				 	numeroLinha++;
				 }
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		    if(type2 == "") {
		    	types = type1.toUpperCase() + " " + "---";
		    }
		    else {
		     types = type1.toUpperCase() + " " + type2.toUpperCase();
		    }
		     return types;
	}
	
	public String[] acessPokemonMoves() {
		
		ArrayList<MoveNames> moveNames = new ArrayList<MoveNames>();
		
		
		String url = this.url;
	    URL obj = null;
		try {
			obj = new URL(url);
		} catch (MalformedURLException e5) {
			// TODO Auto-generated catch block
			e5.printStackTrace();
		}
	    		 
	     HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) obj.openConnection();
		} catch (IOException e4) {
			// TODO Auto-generated catch block
			e4.printStackTrace();
		}
	     // optional default is GET
	     try {
			con.setRequestMethod("GET");
		} catch (ProtocolException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
	     //add request header
	     con.setRequestProperty("User-Agent", "Mozilla/5.0");
	     int responseCode = 0;
		try {
			responseCode = con.getResponseCode();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	     System.out.println("\nSending 'GET' request to URL : " + url);
	     System.out.println("Response Code : " + responseCode);
	     BufferedReader in = null;
		try {
			in = new BufferedReader(
			         new InputStreamReader(con.getInputStream()));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		  	
		  	 
		     String inputLine;
		      
		     
		
		     boolean aux = false;
		     boolean temp = false;
		     int tamanhoStringArray= 0;
	
		     StringBuffer response = new StringBuffer();
		     
		     try {
				while ((inputLine = in.readLine()) != null) {
				 	response.append(inputLine);
				 	String[] splitMove = inputLine.split("\"");
				 if(splitMove.length >= 2) {
				 	if(splitMove[1].compareTo("move") == 0 ) {
				 		temp = true;
				 		tamanhoStringArray++;
				 	}
				 	if(aux == true) {
				 		aux = false;
				 		MoveNames moveName = new MoveNames(splitMove[3]);
				 		moveNames.add(moveName);
				 		System.out.println(splitMove[3]);
				 		
				 	}
				 	if(temp == true) {
				 		temp = false;
				 		aux = true;
				 	}
				 	
				 }
				  
				 }
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		     
			 String[] moves = new String[tamanhoStringArray];
			
			 String auxiliar = "";
			 
			 
			 for(int i = 0; i < tamanhoStringArray ; i++) {
				 
				 auxiliar = moveNames.get(i).getMoveName();
				 System.out.println("Move LIST: "+auxiliar);
				 moves[i] = auxiliar;
				 System.out.println("Move Name: " + moves[i]);
				 System.out.println("***TAMANHO DO I = "+i+ " ***TAMANHO DO ARRAY = " + tamanhoStringArray);	
				 
			 }
				
			 
		     
		     return moves;
	}
	
public String acessUrlImage() {
		
		String urlImage = "";
		String url = this.url;
	    URL obj = null;
		try {
			obj = new URL(url);
		} catch (MalformedURLException e5) {
			// TODO Auto-generated catch block
			e5.printStackTrace();
		}
	    		 
	     HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) obj.openConnection();
		} catch (IOException e4) {
			// TODO Auto-generated catch block
			e4.printStackTrace();
		}
	     // optional default is GET
	     try {
			con.setRequestMethod("GET");
		} catch (ProtocolException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
	     //add request header
	     con.setRequestProperty("User-Agent", "Mozilla/5.0");
	     int responseCode = 0;
		try {
			responseCode = con.getResponseCode();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	     System.out.println("\nSending 'GET' request to URL : " + url);
	     System.out.println("Response Code : " + responseCode);
	     BufferedReader in = null;
		try {
			in = new BufferedReader(
			         new InputStreamReader(con.getInputStream()));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		  	
		  	 
		     String inputLine;
		      
		     
		     //boolean valorNomes = false;
		    
		     StringBuffer response = new StringBuffer();
		     
		     try {
				while ((inputLine = in.readLine()) != null) {
				 	response.append(inputLine);
				 	String[] splitType = inputLine.split("\"");
				    //System.out.println(inputLine);
				 	if(splitType.length >= 4) {
				 		if(splitType[1].compareTo("front_default") == 0 ) {
					 		
				 			urlImage = splitType[3];
					 	}
				 	}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		   

		     return urlImage;
	}


	
	static class MoveNames{
		String moveName;
		
		MoveNames(String moveName){
			this.moveName = moveName;
		}
	  public String getMoveName() {
		  return moveName;
	  }
	  
	  public void setMoveName(String moveName) {
		  this.moveName = moveName;
	  }
	}
}
