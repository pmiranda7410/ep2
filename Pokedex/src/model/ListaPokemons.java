package model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;




public class ListaPokemons {

	ArrayList<Pokemon> listaPokemons = new ArrayList<Pokemon>();
	ArrayList<PokemonTipo> listaPorTipos = new ArrayList<PokemonTipo>();
	
	public ListaPokemons(){
		

		String url = "https://pokeapi.co/api/v2/pokemon/";
	     URL obj = null;
		try {
			obj = new URL(url);
		} catch (MalformedURLException e5) {
			// TODO Auto-generated catch block
			e5.printStackTrace();
		}
	    		 
	     HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) obj.openConnection();
		} catch (IOException e4) {
			// TODO Auto-generated catch block
			e4.printStackTrace();
		}
	     // optional default is GET
	     try {
			con.setRequestMethod("GET");
		} catch (ProtocolException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
	     //add request header
	     con.setRequestProperty("User-Agent", "Mozilla/5.0");
	     int responseCode = 0;
		try {
			responseCode = con.getResponseCode();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	     System.out.println("\nSending 'GET' request to URL : " + url);
	     System.out.println("Response Code : " + responseCode);
	     BufferedReader in = null;
		try {
			in = new BufferedReader(
			         new InputStreamReader(con.getInputStream()));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		  	
		  	 
		     String inputLine;
		     boolean valorNomes = false;
		     ArrayList<Nome> nome = new ArrayList<Nome>();
		     
		     StringBuffer response = new StringBuffer();
		     try {
				while ((inputLine = in.readLine()) != null) {
				 	response.append(inputLine);
				 	
    
				 	nome.add(new Nome(inputLine, inputLine));	
				
				 }
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		     
		     
		     //Os Nomes começam a partir do 6(sendo ele o primeiro nome)
		     //e vão de 4 em 4 para outros nomes indo até o nome 3798 (Último Nome)
		     String primeiroNome = nome.get(6).getNome();
		   //  System.out.println(nome.get(3798).getNome());
		     int z = 5;
		     int i = 6;
		     int j = 7;
		   
		     
		     
		     while(i < 3799) {
		    	 
		    	 int x = z - 5;
		    	 String[] splitNome = nome.get(i).getNome().split("\"");
		    	 String[] splitURL = nome.get(j).getNome().split("\"");
		    	 Pokemon pokemonLista = new Pokemon();
		    	 pokemonLista.setPosicao(x);
		    	 pokemonLista.setNome(splitNome[3]);
		    	 pokemonLista.setUrl(splitURL[3]);
		    	// String[] tipos = typeCadaPokemon(splitURL[3]);
		    	 //pokemonLista.setType1(tipos[0]);
		    	 //pokemonLista.setType2(tipos[1]);
		    	 this.listaPokemons.add(pokemonLista);
		    	 
		    	 //System.out.print(splitNome[1]);
			     //System.out.print(splitNome[2]);
			     //System.out.println(splitNome[3]);
		    	 z = z + 4;
			     i = i + 4;
			     j = j + 4;
		     }
		     
	}
	

	public ArrayList<Pokemon> listaInteira(){
		
		return listaPokemons;
	}
	
	public String[] nomeCadaPokemon() {
		String[] a = new String[listaPokemons.size()];
		String aux;
		
		
	    //String[] nomes = listaPokemons.toArray(new String[listaPokemons.size()]);
	    for(int i = 0; i< 949; i++) {
	    	aux = listaPokemons.get(i).getNome();
	    	a[i] = aux;
	    	System.out.println(a[i]);
	    }
	    return a;
	}
	
	public int indicePokemonPorNome(String nomePokemon) {
		
		int i = -1;
		
		for(int j = 0; j<listaPokemons.size();j++) {
			if(nomePokemon.compareTo(listaPokemons.get(j).getNome().trim()) == 0) {
				i =j;
			}
			
		}
		
		return i;
		
	}
	
	public String[] typeCadaPokemon(String urlPokemon) {
		String[] typeFinal = {"",""};
		String types = "";
		String type1 = "";
		String type2 = "";
		
		String url = urlPokemon;
	    URL obj = null;
		try {
			obj = new URL(url);
		} catch (MalformedURLException e5) {
			// TODO Auto-generated catch block
			e5.printStackTrace();
		}
	    		 
	     HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) obj.openConnection();
		} catch (IOException e4) {
			// TODO Auto-generated catch block
			e4.printStackTrace();
		}
	     // optional default is GET
	     try {
			con.setRequestMethod("GET");
		} catch (ProtocolException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
	     //add request header
	     con.setRequestProperty("User-Agent", "Mozilla/5.0");
	     int responseCode = 0;
		try {
			responseCode = con.getResponseCode();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	     System.out.println("\nSending 'GET' request to URL : " + url);
	     System.out.println("Response Code : " + responseCode);
	     BufferedReader in = null;
		try {
			in = new BufferedReader(
			         new InputStreamReader(con.getInputStream()));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		  	
		  	 
		     String inputLine;
		      
		     
		     //boolean valorNomes = false;
		     int numeroLinha = 0;
		     boolean aux = false;
		     int temp = 0;
		     int temp2 = 0;
		     int typeNumber = 0;
		     StringBuffer response = new StringBuffer();
		     
		     try {
				while ((inputLine = in.readLine()) != null) {
				 	response.append(inputLine);
				 	String[] splitType = inputLine.split("\"");
				    //System.out.println(inputLine);
				 	if(splitType.length >= 2) {
				 		//System.out.println("----------");
				 		//System.out.println("Split 0: " + splitType[0]);
				 		//System.out.println("Split 1: " + splitType[1]);
				 		//System.out.println("Split 2: " + splitType[2]);
				 		//System.out.println("----------");
				 		
				 		if(splitType[1].compareTo("type") == 0 ) {
					 		aux = true;
					 		
					 		temp++;
					 	}
				 		
					 	if(temp == 2){
					 		type1 = splitType[3];
					 		
					 		System.out.println(type1);
					 		temp++;
					 		}
					 	
					 	if(temp2 == 2) {
					 		type2 = splitType[3];
					 		
					 		System.out.println(type2);
					 	temp2++;	
					 	}
					 	
					 	if(aux == true) {
				 			temp++;
				 			aux = false;
				 			temp2++;
				 		}
				 	}
				 	
				 	numeroLinha++;
				 }
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		    if(type2 == "") {
		    	typeFinal[0] =type1;
		    	typeFinal[1] = "";
		    	//types = "( "+type1.toUpperCase() + ")";
		    }
		    else {
		    	typeFinal[0] = type1;
		    	typeFinal[1] = type2;
		    // types = "( "+type1.toUpperCase() + " ) ( " + type2.toUpperCase()+ " )";
		    }
		    
		     return typeFinal;
		
	}
	
	
	public String[] nomeCadaPokemonPorTipo(String tipo) {
	
		int j = 0;
		String[] temp = new String[listaPokemons.size()];
	for(int i = 0; i < listaPokemons.size(); i++ ) {
		
		if(listaPokemons.get(i).getType1Csv().equals(tipo)) {
			temp[j] = listaPokemons.get(i).getNome();
			j ++;
			
		}
		
		else if(listaPokemons.get(i).getType2Csv() != "") {
			
			if(listaPokemons.get(i).getType2Csv().equals(tipo)) {
				temp[j] = listaPokemons.get(i).getNome();
				j ++;
			}
		}
		
	}
		
		String[] listaPorTipo = new String[j];
		String aux = "";
		
		for(int i = 0; i <j ; i++) {
			
			aux = temp[i];
			listaPorTipo[i] = aux;
			
		}
		
		
	
		return listaPorTipo;
	}

	
	
	
	static class Nome{
		String nome;
		String chaves;
		Nome(String nome){
			this.nome = nome;
			
		}
		Nome(String nome, String chaves){
			this.nome = nome;
			this.chaves = chaves;
			
		}
		
		String getNome() {
			return nome;
		}
		String getChaves() {
			return chaves;
		}
	}
	
	
	static class PokemonTipo{
		int numero;
		String nome;
		String tipo1;
		String tipo2;
		
		PokemonTipo(int numero, String tipo1, String tipo2, String nome){
			this.numero = numero;
			this.tipo1 = tipo1;
			this.tipo2 = tipo2;
			this.nome = nome;
		}

		public int getNumero() {
			return numero;
		}

		public String getNome() {
			return nome;
		}
		
		public void setNome(String nome) {
			this.nome = nome;
		}

		
		public void setNumero(int numero) {
			this.numero = numero;
		}

		public String getTipo1() {
			return tipo1;
		}

		public void setTipo1(String tipo1) {
			this.tipo1 = tipo1;
		}

		public String getTipo2() {
			return tipo2;
		}

		public void setTipo2(String tipo2) {
			this.tipo2 = tipo2;
		}
		
		
	
	}
		
		
	}
	
	
	

