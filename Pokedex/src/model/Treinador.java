package model;

import java.util.ArrayList;

public class Treinador {
	public String nome;
	private String nomeUsuario;
	private String senha;
	ArrayList<Name> pokemons = new ArrayList<Name>();

	
	
	public Treinador(String[] nome_nomeUsuario_senha_pokemons){
		int i = 3;
		this.nome = nome_nomeUsuario_senha_pokemons[0];
		this.nomeUsuario = nome_nomeUsuario_senha_pokemons[1];
		this.senha = nome_nomeUsuario_senha_pokemons[2];	
		String line;
		System.out.println("TAMANHO TREINADOR: "+nome_nomeUsuario_senha_pokemons.length);
		while(i < nome_nomeUsuario_senha_pokemons.length) {
			System.out.println("TA NO WHILE: " + i);
			line = nome_nomeUsuario_senha_pokemons[i];
			Name nomePokemon = new Name(line);
			pokemons.add(nomePokemon);
			i++;
		}
		
		
	}

	public String getNome() {
		return nome;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public String getSenha() {
		return senha;
	}
	
	public ArrayList<Name> getPokemons() {
		return pokemons;
	}
	
	public void setPokemons(ArrayList<Name> pokemons) {
		this.pokemons = pokemons;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public String[] getPokemonsList() {
		System.out.println(pokemons.size());
		int tamanho = pokemons.size();
		String[] pokemonsLista = new String [tamanho];
		
		for(int j =0; j< tamanho; j++) {
			pokemonsLista[j] = pokemons.get(j).getName();
			System.out.println(pokemons.get(j).getName());
		}
		
		return pokemonsLista;
	}
	
	static class Name{
		String name;
		
		Name(String name){
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		
		
	}
}
